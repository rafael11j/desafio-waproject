import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import EyeIcon from '@material-ui/icons/PanoramaFishEye';
import AddBox from '@material-ui/icons/AddBox';
import Toolbar from '../../Layout/Toolbar';
import React, { Fragment, memo, useEffect, useState, useCallback } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import api from '../../../services/api';
import IRequest from 'interfaces/models/request';
import RequestDetail from './RequestDetail';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});

const RequestPage = memo((props: {}) => {
  const classes = useStyles(props);
  const [requets, setRquests] = useState([]);
  const [request, setRequest] = useState(Object);
  const [showRequestDetail, setShowRequestDetail] = useState(false);

  const loadRequests = useCallback(() => {
    api.get('/request').subscribe((data: any[]) => {
      setRquests(data);
    });
  }, []);

  useEffect(() => {
    loadRequests();
  }, [loadRequests]);

  const showRequest = (data: IRequest) => {
    setRequest(data);
    setShowRequestDetail(true);
  };

  const removeRequest = (id: string) => {
    api.delete(`/request/${id}`).subscribe(() => {
      loadRequests();
    });
  };

  const changeStateDialog = () => {
    setShowRequestDetail(!showRequestDetail);
  };

  return (
    <Fragment>
      <Toolbar title='Pedidos'></Toolbar>

      {showRequestDetail ? <RequestDetail modalAberto={changeStateDialog} request={request}></RequestDetail> : ''}

      <Grid container spacing={3}>
        <Grid item xs>
          <Button variant='contained' color='primary' startIcon={<AddBox />} href='pedidos_cadastro'>
            Novo Pedido
          </Button>
        </Grid>
      </Grid>

      <br />

      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>Descrição</TableCell>
              <TableCell align='center'>Quantidade</TableCell>
              <TableCell align='center'>Valor</TableCell>
              <TableCell align='center'>Controle</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {requets.map(row => (
              <TableRow key={row.id}>
                <TableCell component='th' scope='row'>
                  {row.description}
                </TableCell>
                <TableCell align='center'>{row.amount}</TableCell>
                <TableCell align='center'>R$ {row.price}</TableCell>
                <TableCell align='center'>
                  <Button
                    variant='contained'
                    color='secondary'
                    startIcon={<EyeIcon />}
                    onClick={() => showRequest(row)}
                  >
                    Vizualizar
                  </Button>
                  &nbsp;&nbsp;
                  <Button
                    variant='contained'
                    color='primary'
                    startIcon={<DeleteIcon />}
                    onClick={() => removeRequest(row.id)}
                  >
                    Remover
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Fragment>
  );
});
export default RequestPage;
