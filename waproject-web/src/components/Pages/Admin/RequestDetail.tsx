import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function RequestDetailPage(props: any) {
  const [open, setOpen] = useState(true);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
      >
        <DialogTitle id='alert-dialog-title'>Detalhe do Pedido</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <strong>Código</strong>: {props.request.id}
          </DialogContentText>
          <DialogContentText id='alert-dialog-description'>
            <strong>Descrição</strong>: {props.request.description}
          </DialogContentText>
          <DialogContentText>
            <strong>Valor Unitário</strong>: R$ {props.request.price}
          </DialogContentText>
          <DialogContentText>
            <strong>Quantidade</strong>: {props.request.amount}
          </DialogContentText>
          <DialogContentText>
            <strong>Total</strong>: R$ {props.request.price * props.request.amount}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.modalAberto} color='primary' autoFocus>
            Fechar
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
